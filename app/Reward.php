<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    static private $_instance = NULL;
    public function __construct() {}

    private function __clone() {}

    static function getInstance() {
        if (self::$_instance == NULL) {
            self::$_instance = new Reward();
        }
        return self::$_instance;
    }
}
