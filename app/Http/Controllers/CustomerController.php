<?php

namespace App\Http\Controllers;
use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function all_customer(){
        //get all category with post include (with methos posts in App\Post.php)
        // $category = Category::with('posts')->get();
        // $customer = Customer::getInstance()->orderBy("id","desc")->get();
        $customer = Customer::getInstance()->orderBy('id', 'desc')->paginate(10);//use for paginate
         //with function user and category in Post.php
        return response()->json([
            'customers' => $customer,
        ],200);
    }
    public function addCustomer(Request $request){
        $this->validate($request,[
            'name'=>'required|min:2|max:50',
            'phone'=>'required|min:10|max:11',
            'level'=>'required',
            'point'=>'required',
            'email'=>'required'
        ]);
        $newCus = Customer::getInstance();
        $request->point!=null?$newCus->point=$request->point:$newCus->point =10;
        $newCus->name = $request->name;
        $newCus->address = $request->address;
        $newCus->level = $request->level;
        $newCus->code_intro = $request->code;
        $newCus->email =$request->email;
        $newCus->phone =$request->phone;
        $newCus->save();
        return ['message'=>'ok'];
    }
    public function updateCustomer(Request $request,$id){
        $customer = Customer::getInstance()->find($id);
        $this->validate($request,[
            'name'=>'required|min:2|max:50',
            'phone'=>'required|min:10|max:11',
            'level'=>'required',
            'point'=>'required',
            'email'=>'required'
        ]);
        $request->point!=null?$customer->point=$request->point:$customer->point =10;
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->level = $request->level;
        $customer->code_intro = $request->code_intro;
        $customer->email =$request->email;
        $customer->phone =$request->phone;
        $customer->save();
        return ['message'=>'ok'];
    }
    public function deleteCus($id){
        $customer = Customer::getInstance()->find($id);
        $customer->delete();
        return ['message'=>'Delete Done'];
    }
    public function editCus($id){
        $CustimerItem = Customer::getInstance()->find($id);
        return response()->json([
            'customers' => $CustimerItem,
        ],200);
    }
}
