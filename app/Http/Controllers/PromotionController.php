<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Messages;
class PromotionController extends Controller
{
    public function allPromotions(){
        //get all category with post include (with methos posts in App\Post.php)
        // $category = Category::with('posts')->get();
        $promotion = Messages::getInstance()->all();
         //with function user and category in Post.php
        return response()->json([
            'promotions' => $promotion,
        ],200);
    }
}
