<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;//import Auth::Library
use Illuminate\Http\Request;
use App\Post;
use App\Category;
// use Image;
use Intervention\Image\ImageManagerStatic as Image;
// use App\Post;
class PostController extends Controller
{
    public function all_post(){
        //get all category with post include (with methos posts in App\Post.php)
        // $category = Category::with('posts')->get();
        $post = Post::with('user','category')->orderBy("id","desc")->get();
         //with function user and category in Post.php
        return response()->json([
            'posts' => $post,
        ],200);
    }
    public function addPost(Request $request){
        $this->validate($request,[
            'title'=>'required|min:2|max:50'
        ]);
        $strpos = strpos($request->photo,';');
        $sub = substr($request->photo,0,$strpos);
        $ex = explode('/',$sub)[1];
        $name =time().".".$ex;
        $img = Image::make($request->photo)->resize(200,200);
        $upload_path = public_path()."/upload/";
        $img->save($upload_path.$name);
        //execute images
        $newPost = Post::getInstance();
        $newPost->title = $request->title;
        $newPost->description = $request->description;
        $newPost->user_id = Auth::id();
        $newPost->cat_id = $request->cat_id;
        $newPost->photos = $name;
        $newPost->save();
        return ['message'=>'ok'];
       
    }
    public function deletePost($id){
        $post = Post::getInstance()->find($id);
        $imagePath = public_path()."/upload/";
        $image = $imagePath.$post->photos;
        if(file_exists($image)){
            @unlink($image);
        }
        $post->delete();
    }
    public function editPost($id){
        $postItem = Post::getInstance()->find($id);
        return response()->json([
            'postItem' => $postItem,
        ],200);
    }
    public function updatePost(Request $request,$id){
        $post = Post::getInstance()->find($id);
        $this->validate($request,[
            'title'=>'required|min:2|max:50'
        ]);
        
        if($request->photo !=$post->photo){
            $strpos = strpos($request->photo,';');
            $sub = substr($request->photo,0,$strpos);
            $ex = explode('/',$sub)[1];
            $name =time().".".$ex;
            $img = Image::make($request->photo)->resize(200,200);
            $upload_path = public_path()."/upload/";
            $img->save($upload_path.$name);
            $imagePath = public_path()."/upload/";
            $image = $imagePath.$post->photos;
            if(file_exists($image)){
                @unlink($image);
            }
            
        }else{
           $name = $post->photo;
        }
        $post->title = $request->title;
            $post->description = $request->description;
            $post->user_id = Auth::id();
            $post->cat_id = $request->cat_id;
            $post->photos = $name;
            $post->save();
        return ['message'=>'ok'];
    }
}
