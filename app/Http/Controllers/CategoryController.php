<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category as category;
class CategoryController extends Controller
{   
    public function addCategory(Request $request){
        $this->validate($request,[
            'cat_name'=>'required|min:2|max:50'
        ]);
        $newCategory = Category::getInstance();
        $newCategory->cat_name = $request->cat_name;
        $newCategory->save();
        return ['message'=>'ok'];
    }
    public function getAllCategory(){
        $Listcategory = Category::getInstance()->all();
        return response()->json([
            'categoryList' => $Listcategory,
        ],200);
    }
    public function deleteCategory($id){
        $category = Category::getInstance()->find($id);
        $category->delete();
    }
    public function editCategory($id){
        $categoryItem = Category::getInstance()->find($id);
        return response()->json([
            'categoryItem' => $categoryItem,
        ],200);
    }
    public function updateCategory(Request $request,$id){
        $category = Category::getInstance()->find($id);
        $this->validate($request,[
            'cat_name'=>'required|min:2|max:50'
        ]);
        $category->cat_name = $request->cat_name;
        $category->save();
        return ['message'=>'ok'];
    }
    public function selected_category($ids){
        $all_id = explode(',',$ids);//make array id
        foreach ($all_id as $id){
            $category =  Category::getInstance()->find($id);
            $category->delete();
        }                                                                                                 
    }
}
