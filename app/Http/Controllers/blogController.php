<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;//import Auth::Library
use Illuminate\Http\Request;
use App\Post;
use App\Category;
class blogController extends Controller
{
    public function get_all_blogpost(){
        $post = Post::with('user','category')->orderBy("id","desc")->get();
         //with function user and category in Post.php
        return response()->json([
            'posts' => $post,
        ],200);
    }
    public function postDetail($id){
        $blogPostItem = post::with('user','category')->get()->find($id);
        return response()->json([
            'postDetail' => $blogPostItem,
        ],200);
    }
    public function sideBarCategory(){
        $Listcategory = Category::getInstance()->all();
        return response()->json([
            'categoryList' => $Listcategory,
        ],200);
    }
    public function getPostByCategory($id){
        $post = Post::with('user','category')->where('cat_id',$id)->orderBy("id","desc")->get();
        //with function user and category in Post.php
        return response()->json([
           'posts' => $post,
        ],200);
    }
    public function searchPost(){
        $search = \Request::get('result');
        if($search!=null){
            $post = Post::with('user','category')
            ->where('title','LIKE',"%$search%")
            ->orWhere('description','LIKE',"%$search%")
            ->get();
            //with function user and category in Post.php
            return response()->json([
               'posts' => $post,
            ],200);
        }else{
            $post = Post::with('user','category')->orderBy("id","desc")->get();
         //with function user and category in Post.php
            return response()->json([
                'posts' => $post,
            ],200);
        }
    }
}    
