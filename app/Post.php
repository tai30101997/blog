<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    static private $_instance = NULL;
    public function __construct() {}

    private function __clone() {}

    static function getInstance() {
        if (self::$_instance == NULL) {
            self::$_instance = new Post();
        }
        return self::$_instance;
    }
    //set relationShip post belongto user

    public function user(){
        return $this->belongsTo(User::class,"user_id");
    }
    //set relationShip post belongto category

    public function category(){
        return $this->belongsTo(Category::class,"cat_id");
    }
}
