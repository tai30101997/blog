<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Reward;

class Customer extends Model
{
    static private $_instance = NULL;
    public function __construct() {}

    private function __clone() {}

    static function getInstance() {
        if (self::$_instance == NULL) {
            self::$_instance = new Customer();
        }
        return self::$_instance;
    }
    public function rewards(){
        //get reward of customer     
        return $this->hasMany(Reward::class,"reward_id");
    }
}
