<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('public/index');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {
    Route::post('/add-category','CategoryController@addCategory'); 
    Route::post('update-category/{id}','CategoryController@updateCategory'); 
    Route::get('category','CategoryController@getAllCategory');//redirect to getAllCategory =>return json
    Route::get('delete-cat/{id}','CategoryController@deleteCategory');
    Route::get('edit-category/{id}','CategoryController@editCategory');
    Route::get('deletecategory/{id}','CategoryController@selected_category');
    //post route 
    Route::get('post','PostController@all_post');
    Route::post('/add-post','PostController@addPost');
    Route::get('/delete-post/{id}','PostController@deletePost'); 
    Route::get('edit-post/{id}','PostController@editPost');
    Route::post('update-post/{id}','PostController@updatePost'); 
    //customer
    Route::get('customer','CustomerController@all_customer');
    Route::post('/add-customer','CustomerController@addCustomer');
    Route::post('update-customer/{id}','CustomerController@updateCustomer'); 
    Route::get('/edit-customer/{id}','CustomerController@editCus'); 
    Route::get('/delete-customer/{id}','CustomerController@deleteCus'); 
    //promotions
    Route::get('/promotions','PromotionController@allPromotions');
});

//blogSpot

Route::get('/blog','blogController@get_all_blogpost');
Route::get('post-detail/{id}','blogController@postDetail');
Route::get('/sidebar-category','blogController@sideBarCategory');
Route::get('/postByCatid/{id}','blogController@getPostByCategory');
Route::get('/searchpost','blogController@searchPost');
// change route login
// Route::get('/cchackcl','Auth\LoginController@showLoginForm');
// //not use login by default => use cchackcl routes
// Route::match(['get', 'post'], 'login', function(){
//     return redirect('/');
// });
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});










