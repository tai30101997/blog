<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rewards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewards', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->nullable();
            $table->double('point')->nullable();
            $table->boolean('type')->default(true);
            $table->text('description')->nullable();
            $table->string('title')->nullable();
            $table->string('photos')->nullable();
            $table->double('quality')->nullable();;
            $table->string('customer_code')->nullable();
            $table->string('messages_id')->nullable();
            $table->boolean('seen')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {  
        {
            Schema::dropIfExists('Rewards');
        }
    }
}
