<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PromotionStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('promotion_stock', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->nullable();
            $table->boolean('type')->default(true);
            $table->text('description')->nullable();
            $table->string('title')->nullable();
            $table->double('input');
            $table->double('output');
            $table->string('messages_id');
            $table->string('reward_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        {
            Schema::dropIfExists('promotion_stock');
        }    
    }
}
