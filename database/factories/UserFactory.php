<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'cat_name' => $faker->name,
    ];
});
$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'cat_id'  =>rand(1,10),
        'user_id' => rand(1,10),
        'comment_id' => rand(1,10),
        'title' => $faker->sentence,
        'description' =>$faker->paragraph,
        'photos' => $faker->imageUrl,
    ];
});
$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'reward_id'  =>rand(1,10),
        'name' => $faker->name,
        'email'=>$faker->unique()->safeEmail,
        'code_intro' => $faker->sentence,
        'phone' => $faker->phoneNumber,
        'level' => $faker->name,
        'address' => $faker->address,
        'point' =>rand(10,100),
    ];
});
$factory->define(App\Reward::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'quality' =>rand(1,10),
        'photos' => $faker->imageUrl,
        'customer_code' => $faker->name,
        'description' =>$faker->paragraph,
        'point' => rand(10,100),
        'messages_id'=>rand(1,10),
    ];
});
