
require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
//support editor
import 'v-markdown-editor/dist/index.css';
import Editor from 'v-markdown-editor'
import '@fortawesome/fontawesome-free/js/all.js'
// global register
Vue.use(Editor);
import Swal from 'sweetalert2/dist/sweetalert2.js'

import 'sweetalert2/src/sweetalert2.scss'
window.Swal = Swal;
//beauty alert
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
window.Toast = Toast;
Vue.use(VueRouter)
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('admin-home', require('./components/admin/AdminHomeBlade.vue').default);
Vue.component("admin-main", () => import("./components/admin/adminMaster.vue")); //fix error not recongnize component
Vue.component("home-main", () => import("./components/public/publicMaster.vue")); //fix error not recongnize component
Vue.component('pagination', require('laravel-vue-pagination'));//pagination

import {routes} from './routes'
import {Form,HasError,AlertError} from 'vform'
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
//support Vuex

Vue.use(Vuex)
import Vuex from 'vuex'
import storeData from  "./components/store/index"
//support moment.js
import {filters} from "./filter.js"
const store = new Vuex.Store(
  storeData
);
const router = new VueRouter({
    routes: routes,
    mode: 'hash'
});
const app = new Vue({
    el: '#app', 
    router: router,
    store
});
