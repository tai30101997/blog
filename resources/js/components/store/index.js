import Axios from "axios";
export default {
    state:{
        category:[],
        post:[],
        blogPost:[],
        postDetail:[],
        categorysidebar:[],
        listCustomer:[],
        listPromotion:[],
    },
    getters:{
        getCategory(state){
            return state.category;
        },
        getPost(state){
            return state.post;
        },
        getblogPost(state){
            return state.blogPost;
        },
        getPostDetail(state){
            return state.postDetail;
        },
        getAllCategorySideBar(state){
            return state.categorysidebar;
        },
        getAllCustomer(state){
            return state.listCustomer;
        }
    },
    actions:{
        allCategory(context){
            //use axios call api through route in web.php 
            axios.get('/sidebar-category').then((response)=>{
               context.commit('categoryIs',response.data.categoryList);
            });
        },
        getAllPost(context){
            //use axios call api through route in web.php 
            axios.get('/post').then((response)=>{
               context.commit('allPost',response.data.posts);
            });
        },
        getAllPromotion(context){
            axios.get('/promotion').then((response)=>{
                context.commit('allPromotion',response.data.promotions)
            });

        },
        getAllBlogPost(context){
            //use axios call api through route in web.php 
            axios.get('/blog').then((response)=>{
               context.commit('allBlogPost',response.data.posts);
            });
        },
        getBlogPostDetail(context,payload){
            axios.get('/post-detail/'+payload).then((response)=>{
                context.commit('postDetail',response.data.postDetail);
            });
        },
        allCategoryinSideBar(context){
            //use axios call api through route in web.php 
            axios.get('/category').then((response)=>{
               context.commit('allCategoryinSide',response.data.categoryList);
            });
        },
        getAllPostByCatid(context,payload){
            axios.get('/postByCatid/'+payload).then((response)=>{
                context.commit('getPostbycatId',response.data.posts);
            });
        },
        realSearch(context,payload){
            axios.get('/searchpost?result='+payload).then((response)=>{
                context.commit('getSearchBox',response.data.posts);
            })
        },
        getCustomer(context){
            //use axios call api through route in web.php 
            axios.get('/customer').then((response)=>{
               context.commit('allCustomer',response.data.customers);
            });
        },
    },
    mutations:{
        categoryIs(state,data){
            return state.category = data;
        },
        allPost(state,payload){
            return state.post=payload;
        },
        getPostbycatId(state,payload){
            return state.blogPost=payload;
        },
        allBlogPost(state,payload){
            return state.blogPost=payload;
        },
        allPromotion(state,payload){
            return state.listPromotion=payload;
        },
        postDetail(state,payload){
            return state.postDetail=payload;
        },
        allCategoryinSide(state,payload){
            return state.categorysidebar=payload;
        },
        getSearchBox(state,payload){
            return state.blogPost = payload;
        },
        allCustomer(state,payload){
            return state.listCustomer = payload;
        }
    }
}
