import adminHome from './components/admin/AdminHomeBlade.vue'
import posList from './components/admin/post/listPost.vue'
import editPost from './components/admin/post/editPost.vue'
import categoryList from './components/admin/category/List.vue'
import addPost from './components/admin/post/addPost.vue'
import addCategory from './components/admin/category/newCategory.vue'
import editCategory from './components/admin/category/edit.vue'
//customer
import customer from './components/admin/customer/listCustomer.vue'
import addCustomer from './components/admin/customer/addCustomer.vue'
import editCustomer from './components/admin/customer/editCustomer.vue'
//front-end Component
import publicHome from './components/public/publicHome.vue'
import blogPost from './components/public/blog/blogPost.vue'
import postDetail from './components/public/blog/singleBlog.vue'
//promotion component
import listPromotion from './components/admin/promotion/listPromotion.vue'
export const routes = [
        { 
            path: '/home',
            name:'home',
            component: adminHome 
        },
        { 
            path: '/category', 
            name:'list',
            component: categoryList  
        },
        { 
            path: '/add-category', 
            name:'new',
            component: addCategory  
        },
        { 
            path: '/edit-category/:categoryid', 
            name:'edit',
            component: editCategory  
        },
        //post Route
        { 
            path: '/post', 
            name:'post',
            component: posList  
        },
        { 
            path: '/edit-post/:postid', 
            name:'editPost',
            component: editPost  
        },
        { 
            path: '/add-post', 
            name:'newPost',
            component: addPost  
        },
        //customer Route
        {
            path: '/customer', 
            name:'customer',
            component: customer  
        },
        {
            path: '/add-customer',
            name:'newcus',
            component:addCustomer
        },
        {
            path:'/edit-customer/:customerId',
            name:'editcus',
            component:editCustomer
        },
        //promotion 
        {
            path:'/promotion',
            name:'promotion',
            component:listPromotion
        },
        //fontend route
        {
            path: '/', 
            name:'publicHome',
            component: publicHome   
        },
        {
            path: '/blog', 
            name:'blogPost',
            component: blogPost   
        },
        { 
            path: '/post-detail/:postid', 
            name:'detail',
            component: postDetail  
        },
        { 
            path: '/category/:postid', 
            component: blogPost  
        },
    ]
    